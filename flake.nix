{
  description = "A flake that provides overlays with common python packages for geometric maching learning";

  outputs = inputs: { inherit (import ./packages) overlayForPython; };

  # Example usage:
  # 1. Add this flake to inputs: inputs.geoml.url = ...;
  # 2. For example, to use kornia with python3.7 on 1080ti, use:
  #       pkgs = import nixpkgs {
  #         inherit system;
  #         pyVer = "37";
  #         py = "python${pyVer}";
  #         overlays = [
  #           (geoml.overlayFor { inherit pyVer; cudaArchList = ["6.1"]; })
  #         ];
  #       };
  #       python-with-packages = pkgs.${py}.withPackages (pypkgs: with pypkgs; [ kornia ])
}

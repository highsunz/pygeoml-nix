{ source, lib, pypkgs }: with pypkgs; buildPythonPackage {
  inherit (source) pname version src;
  propagatedBuildInputs = [ numpy ];
  doCheck = false;

  meta = {
    homepage = "https://github.com/taxpon/pymesh";
    license = lib.licenses.mit;
    description = "Library for manipulating (Translate, Rotate and Scale) 3D data using numpy. Currently, this library supports STL & OBJ.";
  };
}

{ source, cudaArchList, lib, pypkgs
, which
, cudatoolkit
, symlinkJoin
}: with pypkgs; let
  cudatoolkit-unsplit = symlinkJoin {
    name = "${cudatoolkit.name}-unsplit";
    paths = [ cudatoolkit.out cudatoolkit.lib ];
  };
in buildPythonPackage rec {
  inherit (source) pname version src;
  sourceRoot = "${src.name}/lib/pointops";
  CUDA_HOME = "${cudatoolkit-unsplit}";
  TORCH_CUDA_ARCH_LIST = builtins.toString (map (s: "${s}+PTX") cudaArchList);
  # References for below replacements:
  # REF: <https://stackoverflow.com/questions/55919123/cuda-for-pytorch-cuda-c-stream-and-state>
  # REF: <https://github.com/hyangwinter/flownet3d_pytorch/issues/8#issuecomment-663925414>
  # `at::cuda::getCurrentCUDAStream()` should be used without arguments
  postPatch = ''
    for i in $(find . -name '*.cu' -o -name '*.cpp' -o -name '*.h' -o -name '*.cc'); do
      substituteInPlace $i \
        --replace 'THC/THC' 'ATen/ATen' \
        --replace 'AT_CHECK' 'TORCH_CHECK' \
        --replace 'THCudaCheck' 'C10_CUDA_CHECK' \
        --replace 'THCState_getCurrentStream(state)' 'at::cuda::getCurrentCUDAStream()' \
        --replace 'at::cuda::getCurrentCUDAStream(state)' 'at::cuda::getCurrentCUDAStream()'
      sed -e '/\bTHCState\b/d' -i $i
    done
  '';
  nativeBuildInputs = [ which ninja ];
  buildInputs = [ pytorch-bin ];
  doCheck = false;

  meta = {
    homepage = "https://github.com/dvlab-research/Stratified-Transformer";
  };
}

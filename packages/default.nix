let
  mapPackage = f: with builtins; listToAttrs (map
    (name: { inherit name; value = f name; })
    (filter
        (v: v != null)
        (attrValues (mapAttrs
          (path: type: if type == "directory" && path != "_sources" then path else null)
          (readDir ./.)
        ))
    )
  );
in {
  # NOTE: To find appropriate cudaArch to put in `cudaArchList` below, consult <https://developer.nvidia.com/cuda-gpus>.
  # REF: <https://github.com/pytorch/extension-cpp/issues/71#issuecomment-824919813>
  # E.g., for 1080ti, pass cudaArchList = ["6.1"].
  overlayForPython = config@{ pyVer ? "39", cudaArchList ? ["6.1"] }: (final: prev:
    {
      "python${pyVer}" = prev."python${pyVer}".override {
        packageOverrides = finalScope: prevScope: mapPackage (name: let
          generated = (import ./_sources/generated.nix) { inherit (final) fetchurl fetchgit fetchFromGitHub; };
          package = import ./${name};
          args = with builtins; intersectAttrs (functionArgs package) {
            inherit cudaArchList;
            source = generated.${name};
            pypkgs = final."python${pyVer}".pkgs;  # NOTE: use final.${scope} here so that packages from this flake can depend on each other
          };
        in
          prev.callPackage package args  # `callPackage` is identical from final/prev, using either is fine
        );
      };
    }
  );
}

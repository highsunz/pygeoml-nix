{ source, lib, pypkgs }: with pypkgs; buildPythonPackage {
  inherit (source) pname version src;

  meta = {
    homepage = "https://github.com/bshillingford/python-torchfile";
    description = "Deserialize Lua torch-serialized objects from Python";
    license = lib.licenses.bsd3;
  };
}

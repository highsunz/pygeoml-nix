{ source, lib, pypkgs
, which
}: with pypkgs; buildPythonPackage {
  inherit (source) pname version src;

  nativeBuildInputs = [ which ];
  propagatedBuildInputs = [ pytorch-bin scipy ];
  doCheck = false;

  meta = {
    homepage = "https://github.com/rusty1s/pytorch_sparse";
    description = "PyTorch Extension Library of Optimized Autograd Sparse Matrix Operations";
    license = lib.licenses.mit;
  };
}

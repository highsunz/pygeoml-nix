{ source, lib, pypkgs }: with pypkgs; buildPythonPackage {
  inherit (source) pname version src;

  postPatch = ''
    substituteInPlace setup.py \
      --replace 'PyOpenGL==3.1.0' 'PyOpenGL'
  '';
  propagatedBuildInputs = [
    freetype-py
    imageio
    numpy
    pillow
    pyglet
    pyopengl
    scipy
    six
    trimesh
    networkx
  ];
  doCheck = false;

  meta = {
    homepage = "https://github.com/mmatl/pyrender";
    license = lib.licenses.mit;
    description = "Easy-to-use glTF 2.0-compliant OpenGL renderer for visualization of 3D scenes.";
  };
}

{ source, lib, pypkgs }: with pypkgs; buildPythonPackage {
  inherit (source) pname version src;
  propagatedBuildInputs = [ pytorch-bin graphviz ];
  doCheck = false;

  meta = {
    homepage = "https://github.com/szagoruyko/pytorchviz";
    license = lib.licenses.mit;
    description = "A small package to create visualizations of PyTorch execution graphs";
  };
}

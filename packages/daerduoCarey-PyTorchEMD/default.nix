{ source, cudaArchList, lib, pypkgs
, which
, cudatoolkit
, symlinkJoin
}: with pypkgs; let
  cudatoolkit-unsplit = symlinkJoin {
    name = "${cudatoolkit.name}-unsplit";
    paths = [ cudatoolkit.out cudatoolkit.lib ];
  };
in buildPythonPackage {
  inherit (source) pname version src;
  CUDA_HOME = "${cudatoolkit-unsplit}";
  TORCH_CUDA_ARCH_LIST = builtins.toString (map (s: "${s}+PTX") cudaArchList);
  postPatch = ''
    substituteInPlace cuda/emd_kernel.cu \
      --replace 'THC/THC' 'ATen/ATen' \
      --replace 'AT_CHECK' 'TORCH_CHECK' \
      --replace 'THCudaCheck' 'C10_CUDA_CHECK'
  '';
  nativeBuildInputs = [ which ninja ];
  buildInputs = [ pytorch-bin ];
  doCheck = false;

  meta = {
    homepage = "https://github.com/daerduoCarey/PyTorchEMD";
    license = lib.licenses.mit;
    description = "PyTorch Wrapper for Earth-Mover-Distance (EMD) for 3D point cloud regression";
  };
}

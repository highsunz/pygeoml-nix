{ source, lib, pypkgs }: with pypkgs; buildPythonPackage {
  inherit (source) pname version src;

  postPatch = ''
    substituteInPlace setup.py \
      --replace 'sklearn' 'scikit-learn'
  '';

  propagatedBuildInputs = [
    scikitimage
    scikit-learn
    pyrender
    pyopengl
  ];

  meta = {
    homepage = "https://github.com/marian42/mesh_to_sdf";
    license = lib.licenses.mit;
    description = "Calculate signed distance fields for arbitrary meshes";
  };
}

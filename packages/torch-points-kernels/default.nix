{ source, lib, pypkgs, symlinkJoin, cudaArchList
, which
, ninja
, cudatoolkit
}: with pypkgs; let
  cudatoolkit-unsplit = symlinkJoin {
    name = "${cudatoolkit.name}-unsplit";
    paths = [ cudatoolkit.out cudatoolkit.lib ];
  };
in buildPythonPackage {
  inherit (source) pname version src;

  FORCE_CUDA = (builtins.length cudaArchList) > 0;
  CUDA_HOME = "${cudatoolkit-unsplit}";
  TORCH_CUDA_ARCH_LIST = builtins.toString (map (s: "${s}+PTX") cudaArchList);

  nativeBuildInputs = [ which ninja ];
  propagatedBuildInputs = [
    numpy
    numba
    scikit-learn
    pytorch-bin
  ];
  postPatch = ''
    substituteInPlace setup.py \
      --replace 'numpy<1.20' 'numpy'
  '';
  doCheck = false;

  meta = {
    homepage = "https://github.com/nicolas-chaulet/torch-points-kernels";
    description = "Pytorch kernels for spatial operations on point clouds";
    license = lib.licenses.mit;
  };
}

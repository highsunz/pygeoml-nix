{ source, lib, pypkgs
, which
}: with pypkgs; buildPythonPackage {
  inherit (source) pname version src;

  nativeBuildInputs = [ which ];
  propagatedBuildInputs = [ pytorch-bin ];
  doCheck = false;

  meta = {
    homepage = "https://github.com/rusty1s/pytorch_scatter";
    description = "PyTorch Extension Library of Optimized Scatter Operations";
    license = lib.licenses.mit;
  };
}

{ source, lib, pypkgs }: with pypkgs; buildPythonPackage {
  inherit (source) pname version src;

  propagatedBuildInputs = [
    pytorch-bin
    six
    future
    visdom
  ];

  meta = {
    homepage = "https://github.com/pytorch/tnt";
    description = "Simple tools for logging and visualizing, loading and training";
    license = lib.licenses.bsd3;
  };
}

{ source, lib, pypkgs }: with pypkgs; buildPythonPackage {
  inherit (source) pname version src;

  propagatedBuildInputs = [
    jsonpatch
    networkx
    numpy
    pillow
    pyzmq
    requests
    scipy
    six
    tornado
    websocket-client
    torchfile
  ];

  meta = {
    homepage = "https://github.com/fossasia/visdom";
    description = "A flexible tool for creating, organizing, and sharing visualizations of live, rich data. Supports Torch and Numpy.";
    license = lib.licenses.asl20;
  };
}

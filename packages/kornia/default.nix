{ source, lib, pypkgs }: with pypkgs; buildPythonPackage {
  inherit (source) pname version src;
  nativeBuildInputs = [ pytest-runner ];
  propagatedBuildInputs = [ packaging pytorch-bin ];
  doCheck = false;
  meta = {
    homepage = "https://github.com/kornia/kornia";
    license = lib.licenses.asl20;
    description = "Open Source Differentiable Computer Vision Library";
  };
}

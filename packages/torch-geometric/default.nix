{ source, lib, pypkgs
, which
}: with pypkgs; buildPythonPackage {
  inherit (source) pname version src;

  nativeBuildInputs = [ which pyparsing ];
  propagatedBuildInputs = [ 
    pytorch-bin
    scikit-learn
    tqdm
    pandas
    jinja2
  ];
  doCheck = false;

  meta = {
    homepage = "https://github.com/pyg-team/pytorch_geometric";
    description = "Graph Neural Network Library for PyTorch";
    license = lib.licenses.mit;
  };
}
